# Scrumptious

Scrumptious is a simple recipe management application that allows users to create, read, update, and delete recipes for others to share!

## Technologies Used

Python, Django, HTML, CSS

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run "python -m venv .venv" to create a new virtual environment
4. Run "./.venv/Scripts/Activate.ps1" to activate the virtual environment
5. Run "python manage.py createsuperuser your-username" to create a username
6. Run "python manage.py runserver" to run the django server
7. In your browser, go to localhost:8000 to view the website and enjoy
