# Generated by Django 4.1.7 on 2023-02-28 17:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0008_alter_recipestep_recipe"),
    ]

    operations = [
        migrations.AlterField(
            model_name="recipestep",
            name="recipe",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="steps",
                to="recipes.recipe",
            ),
        ),
    ]
